class SignUpController < ApplicationController
def sign_up_page
@user = User.new
end

def receive
	@user = User.new(user_params)
	if(@user.save)
		flash[:success] = "Welcome to Splittr!"
		redirect_to :action => "profile", :id => @user.id
	else
		render 'sign_up_page'
	end
end

	def profile
	end

	private
    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end
end
